package ua.kpi.tef;

import ua.kpi.tef.model.Model;
import ua.kpi.tef.model.Range;
import ua.kpi.tef.model.Statistic;

import java.util.Scanner;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public class Controller {

    // Constructor
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model  = model;
        this.view = view;
    }

    // The Work method
    public void processUser(){
        Scanner sc = new Scanner(System.in);
        view.printWelcomeMessage();
        Statistic statistic;
        do {
            view.printSeparator();
            Range range = model.getCurrentRange();
            if (range.getMinValue() == range.getMaxValue()) {
                view.printLostText(range.getMaxValue());
                return;
            }
            statistic = model.processValue(inputIntValueWithScanner(sc, range));
            model.getSteps().forEach(item -> view.printStatisticLine(
                    item.getRange().getMinValue(),
                    item.getRange().getMaxValue(),
                    item.getGuess(),
                    View.RESULT_MAP.get(item.getResult())));
        } while (statistic.getResult() != 0);
        view.printWonText();
    }

    // The Utility methods
    public int inputIntValueWithScanner(Scanner sc, Range range) {
        int val;
        do {
            view.printMakeGuess(range.getMinValue(), range.getMaxValue());
            while(!sc.hasNextInt()) {
                view.printErrorInInput();
                sc.next();
                view.printMakeGuess(range.getMinValue(), range.getMaxValue());
            }
            val = sc.nextInt();
            if (!range.isInRange(val))
                view.printInputNotInRange(val);
            else
                break;
        } while (true);
        return val;
    }

}
