package ua.kpi.tef;

import ua.kpi.tef.model.Model;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public class Main {

    public static void main(String[] args) {
        // Initialization
        Model model = new Model();
        View view = new View(System.out::print);
        Controller controller = new Controller(model, view);
        // Run
        controller.processUser();
    }

}
