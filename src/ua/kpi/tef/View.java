package ua.kpi.tef;

import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public class View {
    // Text's constants
    public static final String WELCOME_TEXT =
            "Welcome to guess game! You need to guess the number between 0 and 100 (inclusive)!";
    public static final String INPUT_NUMBER_TEXT = "Make guess, input an integer between %d and %d > ";
    public static final String WRONG_INPUT_INT_DATA = "Oops, you've entered somewhat unexpected, try again please!";
    public static final String INPUT_DATA_NOT_IN_RANGE = "You've entered the value out of range -> %d, try again please!";
    public static final String STATISTIC_LINE = "Range from %d to %d, you guessed %d, the number is %s";
    public static final String WON_TEXT = "You won! My congratulations!!!";
    public static final String LOST_TEXT = "You lost! Only one number left -> %d!";
    public static final Map<Integer,String> RESULT_MAP =  Map.of(
            -1, "smaller",
            0, "equal",
            1, "bigger");
    public static final String SEPARATOR_LINE = "~".repeat(60);

    private final Consumer<String> output;

    public View(Consumer<String> output) {
        this.output = output;
    }

    public void printlnMessage(String message) {
        output.accept(message + System.lineSeparator());
    }

    public void printMessage(String message) {
        output.accept(message);
    }

    public void printWelcomeMessage() {
        printlnMessage(WELCOME_TEXT);
    }

    public void printMakeGuess(int minVal, int maxVal) {
        printMessage(String.format(INPUT_NUMBER_TEXT, minVal, maxVal));
    }

    public void printErrorInInput() {
        printlnMessage(WRONG_INPUT_INT_DATA);
    }

    public void printInputNotInRange(int value) {
        printlnMessage(String.format(INPUT_DATA_NOT_IN_RANGE, value));
    }

    public void printStatisticLine(int minVal, int maxVal, int guess, String comparison) {
        printlnMessage(String.format(STATISTIC_LINE, minVal, maxVal, guess, comparison));
    }

    public void printWonText() {
        printlnMessage(WON_TEXT);
    }

    public void printLostText(int value) {
        printlnMessage(String.format(LOST_TEXT, value));
    }

    public void printSeparator() {
        printlnMessage(SEPARATOR_LINE);
    }

}
