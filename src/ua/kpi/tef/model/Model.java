package ua.kpi.tef.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public class Model {

    private static class RangeImpl implements Range {
        private final int minValue;
        private final int maxValue;

        public RangeImpl(int minValue, int maxValue) {
            this.minValue = minValue;
            this.maxValue = maxValue;
        }

        @Override
        public int getMinValue() {
            return minValue;
        }

        @Override
        public int getMaxValue() {
            return maxValue;
        }

        @Override
        public boolean isInRange(int value) {
            return value >= minValue && value <= maxValue;
        }
    }
    private static class StatisticImpl implements Statistic {
        private Range range;
        private int guess;
        private int result;

        public StatisticImpl(Range range) {
            this.range = range;
        }

        @Override
        public Range getRange() {
            return range;
        }

        public void setRange(Range range) {
            this.range = range;
        }

        @Override
        public int getGuess() {
            return guess;
        }

        public void setGuess(int guess) {
            this.guess = guess;
        }

        @Override
        public int getResult() {
            return result;
        }

        public void setResult(int result) {
            this.result = result;
        }
    }

    /**
     * MIN_VALUE - lower bound inclusive for random number generation;
     */
    public static final int MIN_VALUE = 0;

    /**
     * MAX_VALUE_PLUS_ONE - upper bound exclusive for random number generation.
     */
    public static final int MAX_VALUE = 100;

    private Range currentRange;

    private final List<Statistic> steps = new ArrayList<>();
    private final int secretNumber = getRandomNumber();


    // The Program logic
    public Statistic processValue(int value) {
        final StatisticImpl statistic = new StatisticImpl(currentRange);
        statistic.setGuess(value);
        statistic.setResult(Integer.compare(secretNumber, value));
        steps.add(statistic);
        if (statistic.getResult() < 0)
            currentRange = new RangeImpl(currentRange.getMinValue(), value - 1);
        else {
            currentRange = new RangeImpl(value + 1, currentRange.getMaxValue());
        }
        return statistic;
    }

    public List<Statistic> getSteps() {
        return Collections.unmodifiableList(steps);
    }

    public Range getCurrentRange() {
        if (currentRange == null)
            currentRange = new RangeImpl(MIN_VALUE, MAX_VALUE);
        return currentRange;
    }

    /**
     * @return random number by given bounds
     */
    public int getRandomNumber() {
        return ThreadLocalRandom.current().nextInt(MIN_VALUE, MAX_VALUE + 1);
    }
}
