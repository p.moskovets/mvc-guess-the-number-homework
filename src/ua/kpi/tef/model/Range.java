package ua.kpi.tef.model;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public interface Range {
    int getMinValue();

    int getMaxValue();

    boolean isInRange(int value);
}
