package ua.kpi.tef.model;

/**
 * Created by Pavlo Moskovets on 16.02.2022.
 */
public interface Statistic {
    Range getRange();

    int getGuess();

    int getResult();
}
